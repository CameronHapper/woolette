using System;

namespace Tasky.Core {
	/// <summary>
	/// Task business object
	/// </summary>
	public class Login {
		public Login ()
		{
		}

        public int ID { get; set; }
		public string Username { get; set; }
		public string Password { get; set; }
		public bool Done { get; set; }	// TODO: add this field to the user-interface
	}
}