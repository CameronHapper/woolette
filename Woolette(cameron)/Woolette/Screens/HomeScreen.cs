using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Tasky.Core;
using TaskyAndroid;

namespace TaskyAndroid.Screens {
	/// <summary>
	/// Main ListView screen displays a list of tasks, plus an [Add] button
	/// </summary>
	[Activity (Label = "Woolette Login", MainLauncher = true, Icon="@drawable/appicon")]			
	//[Activity (Label = "Woolette Login")]			
	public class HomeScreen : Activity {
		Login task = new Login();
		Button cancelDeleteButton;
		EditText notesTextEdit;
		EditText nameTextEdit;
		Button saveButton;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			int taskID = Intent.GetIntExtra("TaskID", 0);
			if(taskID > 0) {
				//task = TaskManager.GetTask(taskID);
			}

			// set our layout to be the home screen
			SetContentView(Resource.Layout.TaskDetails);
			nameTextEdit = FindViewById<EditText>(Resource.Id.NameText);
			notesTextEdit = FindViewById<EditText>(Resource.Id.NameText);
			saveButton = FindViewById<Button>(Resource.Id.SaveButton);

			// find all our controls
			cancelDeleteButton = FindViewById<Button>(Resource.Id.CancelDeleteButton);

			// set the cancel delete based on whether or not it's an existing task
			//cancelDeleteButton.Text = (task.ID == 1 ? "Login With Facebook" : "Login");

			nameTextEdit.Text = task.Username; 
			notesTextEdit.Text = task.Password;

			// button clicks 
			cancelDeleteButton.Click += (sender, e) => { CancelDelete(); };
			saveButton.Click += (sender, e) => { Save(); };
		}

		void Save()
		{
			task.Username = nameTextEdit.Text;
			task.Password = notesTextEdit.Text;
			TaskManager.SaveTask(task);
			Finish();
		}

		void CancelDelete()
		{
			if (task.ID != 0) {
				//TaskManager.DeleteTask(task.ID);
			}
			Finish();
		}
	}


	/*public class HomeScreen : Activity {
		Adapters.TaskListAdapter taskList;
		IList<Login> tasks;
		Button addTaskButton;
		ListView taskListView;
		
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// set our layout to be the home screen
			SetContentView(Resource.Layout.HomeScreen);

			//Find our controls
			taskListView = FindViewById<ListView> (Resource.Id.TaskList);
			addTaskButton = FindViewById<Button> (Resource.Id.AddButton);

			// wire up add task button handler
			if(addTaskButton != null) {
				addTaskButton.Click += (sender, e) => {
					StartActivity(typeof(LoginScreen));
				};
			}
			
			// wire up task click handler
			if(taskListView != null) {
				taskListView.ItemClick += (object sender, AdapterView.ItemClickEventArgs e) => {
					var taskDetails = new Intent (this, typeof (LoginScreen));
					taskDetails.PutExtra ("TaskID", tasks[e.Position].ID);
					StartActivity (taskDetails);
				};
			}
		}
		
		protected override void OnResume ()
		{
			base.OnResume ();

			//tasks = TaskManager.GetTasks();
			
			// create our adapter
			//taskList = new Adapters.TaskListAdapter(this, tasks);

			//Hook up our adapter to our ListView
			//taskListView.Adapter = taskList;
		}
	}*/
}