using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Runtime;
using Android.Views;
using Tasky.Core;
using TaskyAndroid;
using Woolette;
using Android.Util;
using Android.Animation;
using Android.Views.Animations;
using System;
using Java.Lang;
using System.Threading;

namespace TaskyAndroid.Screens {
	[Activity (Label = "Woolette", MainLauncher = true, Icon="@drawable/appicon")]
	public class SpinnerScreen : Activity {
		Login task = new Login();
		TextView SpinnerText;
		private bool spinning = false;
		private int chosenImageId;
		private int width;
		private LinearLayout wheelLayout;
		//image IDS
		public static int[] imageIds = new int[19] {
			Resource.Drawable.coupon_emerald_avocados,
			Resource.Drawable.coupon_emerald_banana,
			Resource.Drawable.coupon_emerald_batteries,
			Resource.Drawable.coupon_emerald_eggs,
			Resource.Drawable.coupon_emerald_grapes,
			Resource.Drawable.coupon_emerald_milk,
			Resource.Drawable.coupon_silver_apples,
			Resource.Drawable.coupon_silver_avocados,
			Resource.Drawable.coupon_silver_banana,
			Resource.Drawable.coupon_silver_batteries,
			Resource.Drawable.coupon_silver_bread_02,
			Resource.Drawable.coupon_silver_eggs,
			Resource.Drawable.coupon_silver_grapes,
			Resource.Drawable.coupon_silver_milk,
			Resource.Drawable.coupon_gold_apples,
			Resource.Drawable.coupon_gold_banana,
			Resource.Drawable.coupon_gold_batteries,
			Resource.Drawable.coupon_gold_eggs,
			Resource.Drawable.coupon_gold_milk
		};
		private static int[] emeraldIds = new int[6]{
			0,1,2,3,4,5
		};
		private static int[] silverIds= new int[8]{
			6,7,8,9,10,11,12,13
		};
		private static int[] goldIds = new int[5]{
			14,15,16,17,18
		};
		private static readonly int WHEELSIZE = 30;
		private int[] chosenImages;
		//Button facebookButton;
		//EditText notesTextEdit;
		//EditText nameTextEdit;
		//ImageButton spinButton;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			
			int taskID = Intent.GetIntExtra("TaskID", 0);
			if(taskID > 0) {
				//task = TaskManager.GetTask(taskID);
			}

			// set our layout to be the home screen
			SetContentView(Resource.Layout.SpinnerScreen);

			wheelLayout = FindViewById<LinearLayout> (Resource.Id.wheelLayout);
			width = (int) (130f *  Resources.DisplayMetrics.Density);


			SpinnerText = (TextView)FindViewById (Resource.Id.textView2);
			SpinnerText.SetTypeface(null,Android.Graphics.TypefaceStyle.Bold);
			var Challenges = FindViewById (Resource.Id.linearLayout2);
			var Rewards = FindViewById (Resource.Id.linearLayout3);
			var Logout = FindViewById (Resource.Id.linearLayout4);
			//var Logout = FindViewById (Resource.Id.linearLayout5);
			if (Challenges != null) {
				Challenges.Click += (sender, e) => {
					StartActivity (typeof(MyChallenges));
				};
			}
			if (Rewards != null) {
				Rewards.Click += (sender, e) => {
					StartActivity (typeof(MyRewards));
				};
			}
			if (Logout != null) {
				Logout.Click += (sender, e) => {
					StartActivity (typeof(LoginScreen));
				};
			}
			
			//nameTextEdit = FindViewById<EditText>(Resource.Id.NameText);
			//notesTextEdit = FindViewById<EditText>(Resource.Id.NotesText);
			ImageButton spinButton = FindViewById<ImageButton>(Resource.Id.SpinButton);

			FlyOutContainer menu = FindViewById<FlyOutContainer> (Resource.Id.FlyOutContainer);
			var menuButton = FindViewById (Resource.Id.MenuButton);
			if (menuButton != null) {
				menuButton.Click += (sender, e) => {
					//FlyOutContainer menu = FindViewById<FlyOutContainer> (Resource.Id.FlyOutContainer);
					if(menu != null){
						menu.AnimatedOpened = !menu.AnimatedOpened;
					}
				};
			}



			spinButton.Click += (sender, e) => {
				//don't allow double presses
				if(spinning) return;

				Random random = new Random();
				int distanceInImages = random.Next(15,25);
				int distance = (int) ((float)distanceInImages * 130f * Resources.DisplayMetrics.Density)+10*distanceInImages;//last bit is for padding
				int duration = (int)((float)distance / 1.25);

				//find chosen 
				chosenImageId = chosenImages[distanceInImages + 1];
				spinning = true;
				ObjectAnimator anim = ObjectAnimator.OfInt(wheelLayout,"left",-distance);
				anim.SetDuration(duration);
				anim.SetInterpolator(new DecelerateInterpolator());
				anim.Start();


				// set up hacky delayed transition
				ThreadPool.QueueUserWorkItem(o=> {
					System.Threading.Thread.Sleep(duration+1000);
					RunOnUiThread(()=>{
						Intent intent = new Intent(this, typeof(WinScreen));
						intent.PutExtra(WinScreen.INTENT_ID,chosenImageId);
						
						spinning = false;
						StartActivity(intent);

					});
				});
				
			};
		}


		private void setupWheel(){
			//setup the wheel
			Random generator = new Random();
			chosenImages = new int[WHEELSIZE];

			//clear wheel
			wheelLayout.RemoveAllViews();
			while (wheelLayout.ChildCount > 0) {
				wheelLayout.RemoveViewAt (0); 
			}


			for (int i = 0; i < WHEELSIZE; i++) {
				ImageView newImageView = new ImageView (this);

				//choose a random image
				double chosen = generator.NextDouble();
				int nextId;
				if (chosen > 0.8) {
					if (chosen > 0.95) {
						//gold
						nextId = imageIds[goldIds[generator.Next(0,goldIds.Length)]];
					} else {
						//silver
						nextId = imageIds[silverIds[generator.Next(0,silverIds.Length)]];
					}
				} else {
					//emerald image
					nextId = imageIds[emeraldIds[generator.Next(0,emeraldIds.Length)]];

				}
				newImageView.SetImageResource (nextId);

				//because it's adding backwards, set the length-i th of the array
				chosenImages[WHEELSIZE-(i+1)] = nextId;

				newImageView.SetMaxWidth(width);
				newImageView.SetAdjustViewBounds (true);
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams (ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
				newImageView.LayoutParameters = lp;

				newImageView.SetPadding (5, 0, 5, 0);

				wheelLayout.AddView (newImageView,0);
				Log.Debug ("layoutAdd", "imageView " + i + " added");

			}
		}

		private void CreatePanel(){
			
				
		}

		void Save()
		{
			//task.Username = nameTextEdit.Text;
			//task.Password = notesTextEdit.Text;
			//TaskManager.SaveTask(task);
			Finish();
		}
		
		void CancelDelete()
		{
			if (task.ID != 0) {
				//TaskManager.DeleteTask(task.ID);
			}
			Finish();
		}

		protected override void OnResume(){
			base.OnResume ();
			System.Threading.Thread.Sleep (50);
			setupWheel ();
		}
	}
}