﻿using System;
using Android.App;
using Android.OS;
using Android.Util;
using Android.Widget;
using TaskyAndroid.Screens;


namespace Woolette
{
	[Activity (Label = "Win")]
	public class WinScreen : Activity
	{
		public static string INTENT_ID = "com.example.xamarin.winscreen.INTENT_ID";

		protected override void OnCreate (Bundle bundle){
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.WinScreen);
			ImageView win = FindViewById<ImageView> (Resource.Id.imageView_win);
			int id = Intent.GetIntExtra (INTENT_ID, 0);
			win.SetImageResource (id);
			Log.Debug ("WinScreen", (Intent.GetIntExtra (INTENT_ID,0).ToString()));
			ImageButton cont = FindViewById<ImageButton>(Resource.Id.continueButton);
			cont.Click += (sender, e) => {
				StartActivity(typeof(SpinnerScreen));
			};
		}

	}
}

