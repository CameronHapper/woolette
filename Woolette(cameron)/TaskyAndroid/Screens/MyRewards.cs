﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Runtime;
using Android.Views;
using Tasky.Core;
using TaskyAndroid;
using Woolette;

namespace TaskyAndroid.Screens {
	[Activity (Label = "My Rewards")]
	public class MyRewards : Activity {
		Login task = new Login();
		TextView MyRewardsText;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// set our layout to be the home screen
			SetContentView(Resource.Layout.MyRewardsScreen);


			MyRewardsText = (TextView)FindViewById (Resource.Id.textView4);
			MyRewardsText.SetTypeface(null,Android.Graphics.TypefaceStyle.Bold);
			var Challenges = FindViewById (Resource.Id.linearLayout2);
			var Spinner = FindViewById (Resource.Id.linearLayout1);
			var Logout = FindViewById (Resource.Id.linearLayout4);
			//var Logout = FindViewById (Resource.Id.linearLayout5);
			if (Challenges != null) {
				Challenges.Click += (sender, e) => {
					StartActivity (typeof(MyChallenges));
				};
			}
			if (Spinner != null) {
				Spinner.Click += (sender, e) => {
					StartActivity (typeof(SpinnerScreen));
				};
			}
			if (Logout != null) {
				Logout.Click += (sender, e) => {
					StartActivity (typeof(LoginScreen));
				};
			}

			FlyOutContainer menu = FindViewById<FlyOutContainer> (Resource.Id.FlyOutContainer);
			var menuButton = FindViewById (Resource.Id.MenuButton);
			if (menuButton != null) {
				menuButton.Click += (sender, e) => {
					//FlyOutContainer menu = FindViewById<FlyOutContainer> (Resource.Id.FlyOutContainer);
					if(menu != null){
						menu.AnimatedOpened = !menu.AnimatedOpened;
					}
				};
			}

		}
	}
}