﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Android.Runtime;
using Android.Views;
using Tasky.Core;
using TaskyAndroid;
using Woolette;
using Android.Util;

namespace TaskyAndroid.Screens {
	[Activity (Label = "My Challenges")]
	public class MyChallenges : Activity {
		Login task = new Login();
		TextView ChallengesText;
		//Button facebookButton;
		//EditText notesTextEdit;
		//EditText nameTextEdit;
		//ImageButton spinButton;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			int taskID = Intent.GetIntExtra("TaskID", 0);
			if(taskID > 0) {
				//task = TaskManager.GetTask(taskID);
			}

			// set our layout to be the home screen
			SetContentView(Resource.Layout.ChallengesScreen);

			ChallengesText = (TextView)FindViewById (Resource.Id.textView3);
			ChallengesText.SetTypeface(null,Android.Graphics.TypefaceStyle.Bold);
			var Spinner = FindViewById (Resource.Id.linearLayout1);
			var Rewards = FindViewById (Resource.Id.linearLayout3);
			var Logout = FindViewById (Resource.Id.linearLayout4);
			//var Logout = FindViewById (Resource.Id.linearLayout5);
			if (Spinner != null) {
				Spinner.Click += (sender, e) => {
					StartActivity (typeof(SpinnerScreen));
				};
			}
			if (Rewards != null) {
				Rewards.Click += (sender, e) => {
					StartActivity (typeof(MyRewards));
				};
			}
			if (Logout != null) {
				Logout.Click += (sender, e) => {
					StartActivity (typeof(LoginScreen));
				};
			}

			//nameTextEdit = FindViewById<EditText>(Resource.Id.NameText);
			//notesTextEdit = FindViewById<EditText>(Resource.Id.NotesText);
			//spinButton = FindViewById<ImageButton>(Resource.Id.SpinButton);

			FlyOutContainer menu = FindViewById<FlyOutContainer> (Resource.Id.FlyOutContainer);
			var menuButton = FindViewById (Resource.Id.MenuButton);
			if (menuButton != null) {
				menuButton.Click += (sender, e) => {
					//FlyOutContainer menu = FindViewById<FlyOutContainer> (Resource.Id.FlyOutContainer);
					if(menu != null){
						Log.Debug("MyChallenge","Menu found");
						
						menu.AnimatedOpened = !menu.AnimatedOpened;
					}
				};
			}
			/*	Button showPopupMenu = FindViewById<Button> (Resource.Id.MenuButton);

			showPopupMenu.Click += (s, arg) => {

				PopupMenu menu = new PopupMenu (this, showPopupMenu);

				// with Android 3 need to use MenuInfater to inflate the menu
				//menu.MenuInflater.Inflate (Resource.Menu.popup_menu, menu.Menu);

				// with Android 4 Inflate can be called directly on the menu
				menu.Inflate (Resource.Menu.popup_menu);

				menu.MenuItemClick += (s1, arg1) => {
					Console.WriteLine ("{0} selected", arg1.Item.TitleFormatted);
				};

				// Android 4 now has the DismissEvent
				menu.DismissEvent += (s2, arg2) => {
					Console.WriteLine ("menu dismissed"); 
				};

				menu.Show ();
			};
			*/

			// find all our controls
			//facebookButton = FindViewById<Button>(Resource.Id.FacebookButton);

			// set the cancel delete based on whether or not it's an existing task
			//facebookButton.Text = (task.ID == 1 ? "Login With Facebook" : "Login");

			//nameTextEdit.Text = task.Username; 
			//notesTextEdit.Text = task.Password;

			// button clicks 
			/*facebookButton.Click += (sender, e) => {
				StartActivity (typeof(HomeScreen));
			};
			loginButton.Click += (sender, e) => {
				StartActivity (typeof(HomeScreen));
			};*/
			//if(spinButton != null) {
			//	spinButton.Click += (sender, e) => {
			//Randomly select a number between 1 and 20.

			//Play an animation *DO LAST*

			//Move it to the Win screen
			//		StartActivity(typeof(WinScreen));
			//	};
			//}
		}

		private void CreatePanel(){


		}

		void Save()
		{
			//task.Username = nameTextEdit.Text;
			//task.Password = notesTextEdit.Text;
			//TaskManager.SaveTask(task);
			Finish();
		}

		void CancelDelete()
		{
			if (task.ID != 0) {
				//TaskManager.DeleteTask(task.ID);
			}
			Finish();
		}
	}
}