package md53005cf664a0a2c5eb2eb865ab6020e6e;


public class MyRewards
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("TaskyAndroid.Screens.MyRewards, TaskyAndroid, Version=1.0.5783.7773, Culture=neutral, PublicKeyToken=null", MyRewards.class, __md_methods);
	}


	public MyRewards () throws java.lang.Throwable
	{
		super ();
		if (getClass () == MyRewards.class)
			mono.android.TypeManager.Activate ("TaskyAndroid.Screens.MyRewards, TaskyAndroid, Version=1.0.5783.7773, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
