package md5866417e73375dc7e8a37e16aa4b0567b;


public class SpinnerScreen
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("TaskyAndroid.Screens.SpinnerScreen, TaskyAndroid, Version=1.0.5783.5941, Culture=neutral, PublicKeyToken=null", SpinnerScreen.class, __md_methods);
	}


	public SpinnerScreen () throws java.lang.Throwable
	{
		super ();
		if (getClass () == SpinnerScreen.class)
			mono.android.TypeManager.Activate ("TaskyAndroid.Screens.SpinnerScreen, TaskyAndroid, Version=1.0.5783.5941, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
