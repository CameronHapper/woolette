package md52a3a81faa88c4553efb4bce6d9930204;


public class WinScreen
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("Woolette.WinScreen, TaskyAndroid, Version=1.0.5783.6999, Culture=neutral, PublicKeyToken=null", WinScreen.class, __md_methods);
	}


	public WinScreen () throws java.lang.Throwable
	{
		super ();
		if (getClass () == WinScreen.class)
			mono.android.TypeManager.Activate ("Woolette.WinScreen, TaskyAndroid, Version=1.0.5783.6999, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
